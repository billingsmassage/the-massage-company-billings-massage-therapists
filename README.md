The Massage Company in Billings, MT, is dedicated to helping people live life with less pain and discomfort. Licensed massage therapists provide a wide range of customized services from deep tissue and relaxation massage therapy to hot stone massage, foot massage, aromatherapy, and more.

Address: 511 North 30th Street, Billings, MT 59101

Phone: 406-318-0752
